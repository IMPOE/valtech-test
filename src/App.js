import axios from "axios";
import './App.css';
import {useEffect, useMemo, useState} from "react";



function App() {

  const [arr, setArr] = useState([])
  const [inp, setInp] = useState("")
  const [arr1, setArr1] = useState([])


useMemo(() => {
  const arr1 =  arr.filter((el) => {
    if (el?.name.includes(inp)){
      return el
    }
  })
  setArr1(arr1)
},[arr,inp])



  const rap = arr1.map((el) => {
    return <ul>{Object.entries(el).map(([key, value]) => {
      if (Array.isArray(value)){
      const a =   value.map((e,index) => {
          return <li key={index} style={{color:"blue"}}>{e}</li>
        })
        return <ul>{a}</ul>
      }
      return <li style={{ color: key === "name" ? "red" : "black" }}>`${key}: ${value}`</li>
    })}</ul>
  })



  useEffect(async () => {
    try {
      const response = await axios.get("https://demo2817758.mockable.io/products")
      setArr((er) => response.data.products)
    } catch (e) {
      e.message()
    }

  },[])

  return (
    <div className="App">
      <input type="text"  value={inp} onChange={(e) => setInp(e.target.value)}/>
      {rap}
    </div>
  );
}

export default App;

